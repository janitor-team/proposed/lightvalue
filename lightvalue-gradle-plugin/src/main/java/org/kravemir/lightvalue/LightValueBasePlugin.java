package org.kravemir.lightvalue;

import org.gradle.api.NamedDomainObjectContainer;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.JavaBasePlugin;

/**
 * Base plugin provides Light Value capabilities (functionality).
 *
 * Recommended usage is via {@link LightValuePlugin}, which setups configuration based on conventions.
 *
 * @see <a href="https://guides.gradle.org/designing-gradle-plugins/#capabilities-vs-conventions">
 *          Designing Gradle plugins: Capabilities vs. conventions</a>
 */
public class LightValueBasePlugin implements Plugin<Project> {

    @Override
    public void apply(final Project project) {
        project.getPlugins().apply(JavaBasePlugin.class);

        NamedDomainObjectContainer<LightValueConfiguration> lightValueContainer = project.container(
                LightValueConfiguration.class,
                name -> {
                    LightValueGeneratorTask task = project.getTasks().create(name + "ValueClasses", LightValueGeneratorTask.class);
                    task.setGroup("build");
                    return new LightValueConfiguration(name, task);
                }
        );
        project.getExtensions().add("lightvalue", lightValueContainer);
    }
}
