package org.kravemir.lightvalue;

%%

%public
%class ModelLexer
%unicode
%type Token
%line

LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f]

Identifier = [:jletter:] [:jletterdigit:]*

%%

/* keywords */
<YYINITIAL> "package"           { return new Token(Token.Type.PACKAGE, yyline); }
<YYINITIAL> "import"            { return new Token(Token.Type.IMPORT, yyline); }
<YYINITIAL> "class"             { return new Token(Token.Type.CLASS, yyline); }
<YYINITIAL> "repeated"          { return new Token(Token.Type.REPEATED, yyline); }
<YYINITIAL> "map"               { return new Token(Token.Type.MAP, yyline); }

<YYINITIAL> {
    /* identifiers */
    {Identifier}                { return new Token(Token.Type.IDENTIFIER, yyline, yytext()); }

    /* operators */
    "."                         { return new Token(Token.Type.DOT, yyline); }
    ","                         { return new Token(Token.Type.COMMA, yyline); }
    ";"                         { return new Token(Token.Type.SEMICOLON, yyline); }
    "{"                         { return new Token(Token.Type.LCURLY, yyline); }
    "}"                         { return new Token(Token.Type.RCURLY, yyline); }
    "<"                         { return new Token(Token.Type.LESS, yyline); }
    ">"                         { return new Token(Token.Type.MORE, yyline); }

    /* whitespace */
    {WhiteSpace}                { /* ignore */ }
}


/* error fallback */
[^]                             { throw new RuntimeException("Illegal character <" +yytext() +">"); }
