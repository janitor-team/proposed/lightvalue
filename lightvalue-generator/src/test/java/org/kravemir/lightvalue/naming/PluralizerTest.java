package org.kravemir.lightvalue.naming;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class PluralizerTest {

    private Pluralizer pluralizer;

    @Before
    public void setUp() throws Exception {
        pluralizer = new Pluralizer();
    }

    @Test
    @Parameters
    public void testPluralize(String singular, String expectedPlural) {
        assertThat(pluralizer.pluralize(singular), is(expectedPlural));
    }

    public Object[] parametersForTestPluralize() {
        return new Object[][]{
                // regular appending: -s
                {"boat", "boats"},
                {"house", "houses"},
                {"cat", "cats"},
                {"river", "rivers"},

                // regular appending: -es
                {"bus", "buses"},
                {"wish", "wishes"},
                {"pitch", "pitches"},
                {"box", "boxes"},

                {"penny", "pennies"},
                {"spy", "spies"},
                {"baby", "babies"},
                {"city", "cities"},
                {"daisy", "daisies"}
        };
    }
}