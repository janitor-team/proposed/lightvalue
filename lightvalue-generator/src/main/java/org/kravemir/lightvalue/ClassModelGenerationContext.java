package org.kravemir.lightvalue;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;
import org.kravemir.lightvalue.model.ClassDef;

public class ClassModelGenerationContext extends ModelGenerationContext {

    private final ModelGenerationContext parent;

    private final ClassDef classDef;

    private final ClassName classTypeName;
    private final ClassName builderTypeName;

    public ClassModelGenerationContext(
            final ModelGenerationContext parent,
            final ClassDef classDef,
            final ClassName classTypeName,
            final ClassName builderTypeName
    ) {
        this.parent = parent;
        this.classDef = classDef;
        this.classTypeName = classTypeName;
        this.builderTypeName = builderTypeName;
    }

    @Override
    public String getPackageName() {
        return parent.getPackageName();
    }

    public ClassDef getClassDef() {
        return classDef;
    }

    public ClassName getClassTypeName() {
        return classTypeName;
    }

    public ClassName getBuilderTypeName() {
        return builderTypeName;
    }

    @Override
    public TypeName getType(String typeRawName) {
        TypeName name = super.getType(typeRawName);

        if(name == null) {
            name = parent.getType(typeRawName);
        }

        return name;
    }

    @Override
    protected String nameThisContext() {
        return "context for " + classDef.getName();
    }
}
