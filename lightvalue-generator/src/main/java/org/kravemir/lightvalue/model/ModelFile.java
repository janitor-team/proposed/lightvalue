package org.kravemir.lightvalue.model;

import java.util.*;

public class ModelFile {

    private String filePackage;
    private Map<String, String> imports = new LinkedHashMap<>();
    private Map<String, ClassDef> classDefMap = new LinkedHashMap<>();

    public String getFilePackage() {
        return filePackage;
    }

    public void setFilePackage(String filePackage) {
        this.filePackage = filePackage;
    }

    public Map<String, String> getImports() {
        return imports;
    }

    public void addImport(String path) {
        String[] splitPath = path.split("\\.");
        String importName = splitPath[splitPath.length-1];

        if(imports.get(importName) != null) {
            throw new RuntimeException(String.format("Import for %s already exists", importName));
        }

        imports.put(importName, path);
    }

    public Collection<ClassDef> getClassDefs() {
        return classDefMap.values();
    }

    public void addClass(ClassDef classDef) {
        if(classDefMap.get(classDef.getName()) != null) {
            throw new RuntimeException(String.format("Import for %s already exists, can't create class with same name", classDef.getName()));
        }

        if(classDefMap.get(classDef.getName()) != null) {
            throw new RuntimeException(String.format("Class %s already exists", classDef.getName()));
        }

        classDefMap.put(classDef.getName(), classDef);
    }

    @Override
    public String toString() {
        return "ModelFile{" +
                "filePackage='" + filePackage + '\'' +
                ", classDefMap=" + classDefMap +
                '}';
    }
}
