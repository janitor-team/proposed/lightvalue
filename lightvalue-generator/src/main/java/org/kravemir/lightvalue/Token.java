package org.kravemir.lightvalue;

public class Token {

    public enum Type {
        PACKAGE,
        IMPORT,
        CLASS,
        REPEATED,
        MAP,
        IDENTIFIER,
        DOT,
        COMMA,
        SEMICOLON,
        LCURLY,
        RCURLY,
        LESS,
        MORE,
    }

    private final Type type;
    private final int line;
    private final String value;

    public Token(Type type, int line) {
        this(type, line, null);
    }

    public Token(Type type, int line, String value) {
        this.type = type;
        this.line = line;
        this.value = value;
    }

    public Type getType() {
        return type;
    }

    public int getLine() {
        return line;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Token{" +
                "type=" + type +
                ", line=" + line +
                ", value='" + value + '\'' +
                '}';
    }
}
